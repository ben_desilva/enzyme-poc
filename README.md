# React-Enzyme-jsdom-mocha P.O.C (in progress) #

This is a React testing P.O.C with Enzyme, jsdom, and mocha (in progress). Articles and code samples were referenced from the Internet in order to save time in making a robust demo that can be evaluated so that we can possibly make a move into the next step which would be to migrate this framework into patlib and then breakfast-web.

#### Run test:
```npm run test:watch```

#### Screenshot:

**Test run:**
![Test run](images/test-run.png)

**Full DOM rendering component wrapper debug console output:**
![Wrapper debug](images/wrapper-debug.png)