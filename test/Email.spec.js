import Email from '../src/Email';

describe('<Email>', () => {
  it('should have an input for the email', () => {
    const wrapper = shallow(<Email/>);
    expect(wrapper.find('input')).to.have.length(1);
  });

  it('should have a button', () => {
    const wrapper = shallow(<Email/>);
    expect(wrapper.find('button')).to.have.length(1);
  });

  it('should have props for handleEmailChange and fetchGravatar', () => {
    const wrapper = shallow(<Email/>);
    expect(wrapper.props().handleEmailChange).to.be.defined;
    expect(wrapper.props().fetchGravatar).to.be.defined;
  });

  it('should use custom defined functions when initating a click', () => {
    let emailValue = 'test@example.com';
    const clickHandler = () => emailValue = 'newemail@example.com';
    const wrapper = mount(<Email fetchGravatar={clickHandler}/>);

    expect(emailValue).to.equal('test@example.com');
    wrapper.find('button').simulate('click');
    expect(emailValue).to.equal('newemail@example.com');
  });

});
