import md5 from 'md5';

import Gravatar from '../src/Gravatar';
import Avatar from '../src/Avatar';
import Email from '../src/Email';

describe('<Gravatar />', () => {
  it('contains an <Avatar/> component', () => {
    const wrapper = mount(<Gravatar/>);
    expect(wrapper.find(Avatar)).to.have.length(1);
  });

  it('contains an <Email/> component', () => {
    const wrapper = mount(<Gravatar/>);
    expect(wrapper.find(Email)).to.have.length(1);
  });

  it('should have an initial email state', () => {
    const wrapper = mount(<Gravatar/>);
    expect(wrapper.state().email).to.equal('someone@example.com');
  });

  it('should have an initial src state', () => {
    const wrapper = mount(<Gravatar/>);
    expect(wrapper.state().src).to.equal('http://placehold.it/200x200');
  });

  it('should update the src state on clicking fetch', () => {
    const wrapper = mount(<Gravatar/>);
    const avatarWrapper = wrapper.find(Avatar);

    //Assert state before click
    expect(wrapper.state('email')).to.equal('someone@example.com');
    expect(wrapper.state('src')).to.equal('http://placehold.it/200x200');

    //Assert props before click
    expect(avatarWrapper.prop('email')).to.equal('someone@example.com');
    expect(avatarWrapper.prop('src')).to.equal('http://placehold.it/200x200');

    wrapper.setState({email: 'ben.desilva@asurion.com'});
    wrapper.find('button').simulate('click');

    //Assert after click
    expect(wrapper.state('email')).to.equal('ben.desilva@asurion.com');
    expect(wrapper.state('src')).to.equal(`http://gravatar.com/avatar/${md5('ben.desilva@asurion.com')}?s=200`);

    //Assert props after click
    expect(avatarWrapper.prop('email')).to.equal('ben.desilva@asurion.com');
    expect(avatarWrapper.prop('src')).to.equal(`http://gravatar.com/avatar/${md5('ben.desilva@asurion.com')}?s=200`);
  });

});