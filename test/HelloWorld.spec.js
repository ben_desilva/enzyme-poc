import HelloWorld from '../src/HelloWorld';

describe('<HelloWorld />', () => {
  it('should render an h1 tag', () => {
    const wrapper = shallow(<HelloWorld />);
    expect(wrapper.type()).to.eql('h1');
  });

  it ('should render content within the <h1> tag', () => {
    const wrapper = shallow(<HelloWorld />);    
    expect(wrapper.text()).to.eql('Hello World');
  })
});